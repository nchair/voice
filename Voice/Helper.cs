﻿using System;
using System.IO;
using System.Media;
using System.Windows.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Voice
{
    public class Helper
    {
        /// <summary>
        ///     获取配置文件
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static JObject GetConfig()
        {
            JObject o = null;
            string jsonFile = "setting.json";
            try
            {
                using (StreamReader file = File.OpenText(jsonFile))
                {
                    using (JsonTextReader reader = new JsonTextReader(file))
                    {
                        o = (JObject) JToken.ReadFrom(reader);
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                throw;
            }

            return o;
        }

        /// <summary>
        ///     文件转Base64
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static string FileToBase64String(string file)
        {
            string result = string.Empty;


            using (FileStream stream = new FileStream(file, FileMode.Open))
            {
                byte[] bt = new byte[stream.Length];
                stream.Read(bt, 0, bt.Length);
                result = Convert.ToBase64String(bt);
            }

            return result;
        }

        /// <summary>
        ///     根据Key获取Jsong的Value
        /// </summary>
        /// <param name="json"></param>
        /// <param name="ReName"></param>
        /// <returns></returns>
        public static string GetJsonForKey(JToken json, string ReName)
        {
            try
            {
                string result = "";
                //这里6.0版块可以用正则匹配
                var node = json.SelectToken("$.." + ReName);
                if (node != null)
                {
                    //判断节点类型
                    if (node.Type == JTokenType.String || node.Type == JTokenType.Integer ||
                        node.Type == JTokenType.Float)
                    {
                        //返回string值
                        result = node.Value<object>().ToString();
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                return "";
            }
        }


        /// <summary>
        ///     播放Base64声音
        /// </summary>
        /// <param name="base64String"></param>
        public static void PlayAlertSoud(SoundPlayer AlertSoundPlayer, string base64String)
        {
            var titleBuffer = Convert.FromBase64String(FileToBase64String("title.wav"));
            using (var ms = new MemoryStream(titleBuffer))
            {
                AlertSoundPlayer.Stream = ms;
                AlertSoundPlayer.PlaySync();
            }

            var audioBuffer = Convert.FromBase64String(base64String);
            using (var ms = new MemoryStream(audioBuffer))
            {
                AlertSoundPlayer.Stream = ms;
                AlertSoundPlayer.PlaySync();
            }
        }
    }
}